package nfa019Projet.Vue.Swing;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import nfa019Projet.CRUD.EmployeDAO;
import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.Modele.Roles.Patient;


public class Swing extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private JButton retour ;
	private JComboBox<String> ordoList;
	private int selectIndex  = 0 ;
	private JPanel p = new JPanel();
;

	public void accueilEtRetour( ) {
	      try {
	        	ClassLoader classloader = Thread.currentThread().getContextClassLoader();
	        	
	        	//retour page précédente
		        InputStream isR = classloader.getResourceAsStream("img/retour.jpg");
		        Image imageR = ImageIO.read(isR);
		        retour = new JButton("retour",new ImageIcon(imageR));
		        retour.setBounds(50, 350, 100, 100);
		        retour.addActionListener(this);
	        	
		        //retour accueil
	            InputStream isS = classloader.getResourceAsStream("img/home.png");
		        Image imageAc = ImageIO.read(isS);
		        accueil = new JButton("acc",new ImageIcon(imageAc));
		        accueil.setBounds(200, 350, 80, 80);
		       accueil.addActionListener(this);
	        }        
		    catch(IOException e){
		        e.printStackTrace();
		    }
	}
	public JButton getaccueil() {
		return accueil;
	}
	public JButton getretour() {
		return retour;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
	  if (e.getSource().equals(accueil)) {
            this.dispose();            
            Application a= new Application();
            a.setVisible(true);
        }
	}
	
	public void comboList(String[] liste, int x , int y , int w , int h) {

	
			ordoList = new JComboBox<String>(liste);
		//	ordoList.addItemListener(this);
			
		    p.setBounds(x,y,w,h);
	      p.add(ordoList);
	       p.setVisible(true);
	       //add(p);
	    
		
	}
	
	public JPanel getpanel() {
		return p;
	}
	

	public void setSelectIndex(int selectIndex) {
		this.selectIndex = selectIndex;
	}
	
	public int getSelectedIndex() {
		return this.selectIndex;
	}

	
	
	public JComboBox<String> getOrdoList(){
		return ordoList;
	}
	


}
