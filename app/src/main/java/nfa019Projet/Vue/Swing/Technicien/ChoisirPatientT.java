package nfa019Projet.Vue.Swing.Technicien;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;


import nfa019Projet.CRUD.EmployeDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

public class ChoisirPatientT extends JFrame implements ActionListener, ItemListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private Swing ar = new Swing();
	private String idTechnicien;
	String idPatient;
	private JButton retour ;

	private EmployeDAO emp;
	private JComboBox<String> ordoList;
	private int selectIndex  = 0 ;
	private String[] ordonnancePatients;
	private List<String> ordonnancePatientsAvecAppareil = new ArrayList<String>();

	
	public ChoisirPatientT(String idT ) {
		this.idTechnicien = idT;
		ar = new Swing();
		emp = new EmployeDAO(SingleConnection.acces,idTechnicien);
		ordonnancePatients = new String[emp.getConsultations().size() + 1 ];
		ordonnancePatients[0] = "------Faites un choix------";
		for (int i = 1 ; i < ordonnancePatients.length ; i++) {	
			ordonnancePatients[i]=	emp.getConsultations().get(i - 1).ordonnanceToString();
			if (ordonnancePatients[i].contains("appareil")) {
				String[]prescriptions = ordonnancePatients[i].split("\n");
				String temp = "";
				for (String p : prescriptions) {
					if(p.contains("appareil")) {
						temp+= p.split(" : ")[1]+ " ";
					}
					
				}
				ordonnancePatientsAvecAppareil.add(  " "+temp);
			}
		}
        String[] itemsArray = new String[ordonnancePatientsAvecAppareil.size()];
		ar.comboList(ordonnancePatientsAvecAppareil.toArray(itemsArray),150,30,200,60);


		ordoList =ar.getOrdoList();
        ordoList.addItemListener(this);
		add(ar.getpanel());



		ar.accueilEtRetour();
		retour = ar.getretour();
		retour.addActionListener(this);
		add(retour);
		accueil = ar.getaccueil();
		accueil.addActionListener(this);
		add(accueil);
		setLayout(null);
		setSize(600, 500);
		setVisible(true);	


	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(accueil)) {
			this.dispose();
			Application a = new Application();
			a.setVisible(true);
		}	
		else if (e.getSource().equals(retour)) {
			this.dispose();
			(new InterfaceTechnicien()).setVisible(true);
		}	
	}

	@Override
	public void itemStateChanged(ItemEvent e) {

		if (e.getSource().equals(ordoList)) {
			System.out.println("index selectionné : "+selectIndex);
			this.selectIndex = ordoList.getSelectedIndex(); 
			idPatient = ordonnancePatientsAvecAppareil.get(this.selectIndex);
			System.out.println(idPatient);
			this.dispose();
			(new ChoixAppareil(idPatient,idTechnicien)).setVisible(true);	

		}
	}


}

