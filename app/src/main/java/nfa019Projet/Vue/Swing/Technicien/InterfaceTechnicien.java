package nfa019Projet.Vue.Swing.Technicien;

import javax.imageio.ImageIO;
import javax.swing.*;

import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Medecin.ChoisirPatient;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.InputStream;


public class InterfaceTechnicien extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private JTextField identifT;
	private String idTechnicien;
	private JButton buttonIdentifT;


    public InterfaceTechnicien () {
    	JLabel l1 = new JLabel("Saisissez votre id");
		l1.setBounds(30, 50, 300, 50);
		l1.setFont(new Font("Serif", Font.PLAIN, 24));
		add(l1);

		identifT = new JTextField(50);
		identifT.setBounds(40, 120,50,50);
		add(identifT);

		buttonIdentifT = new JButton("Validation");
		buttonIdentifT.setBounds(100,120,100,50);
		add(buttonIdentifT);
		buttonIdentifT.addActionListener(this);



		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		InputStream isS = classloader.getResourceAsStream("img/home.png");

		//retour accueil
		try {

			Image imageAc = ImageIO.read(isS);

			accueil = new JButton("agent",new ImageIcon(imageAc));
			accueil.setBounds(200, 350, 80, 80);
			add(accueil);
			accueil.addActionListener(this);
		}        
		catch(IOException e){
			e.printStackTrace();
		}
		setLayout(null);
		setSize(600, 500);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(accueil)) {
			this.dispose();
			Application a= new Application();
			a.setVisible(true);
		}
		if (e.getSource().equals(buttonIdentifT)) {
			idTechnicien = identifT.getText();
			this.dispose();
			(new ChoisirPatientT(idTechnicien)).setVisible(true);

		}
	}
    }

