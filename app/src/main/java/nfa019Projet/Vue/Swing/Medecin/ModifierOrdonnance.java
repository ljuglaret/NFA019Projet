package nfa019Projet.Vue.Swing.Medecin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

public class ModifierOrdonnance extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private JButton retour ;
	
	private JButton changePrescription;
	private JTextField textePrescription = new JTextField(100);
private Swing ar;
	private int selectIndex  = 0 ;

	private String idP;
	private String idMedecin;
	private JComboBox<String> ordoList;
	PatientDAO patient;
	
	
	public ModifierOrdonnance(String idPatient ,String idMedecin) {
		this.idMedecin = idMedecin.strip();
		this.idP = idPatient.strip();
    	patient = new PatientDAO(SingleConnection.acces,idP);

		 ar = new Swing();
        ar.accueilEtRetour();
        retour = ar.getretour();
        retour.addActionListener(this);
        add(retour);
        accueil = ar.getaccueil();
        accueil.addActionListener(this);
        add(accueil);
        int nbOrdonnances;
        try {
     	   nbOrdonnances = patient.consultations(idMedecin).size();
     	   String[] ordonnances = new String[nbOrdonnances + 1];
     	  ordonnances[0] = "------Faites un choix------";
     	  					
     	   for (int i = 1 ; i < nbOrdonnances + 1 ; i++) {	
 				ordonnances[i]=	"Ordonnance du "+patient.consultations(idMedecin).get(i-1).getDate();
 			}
     	   ar.comboList(ordonnances,30,50,200,60);

        } catch(SQLException e) {
     	   e.printStackTrace();
        }
        ordoList = ar.getOrdoList();
      // ordoList.addItemListener(this);
        ordoList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              selectIndex = ordoList.getSelectedIndex() - 1 ;
              System.out.println(selectIndex);
          	try {
				textePrescription.setText( patient.consultations(idMedecin).get(selectIndex  ).ordonnanceToString());
			    changePrescription.addActionListener(this);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
            }
          });
		add(ar.getpanel());
	       
		
		textePrescription.setBounds(100,150,200,30);
		changePrescription = new JButton("change ordonnance");
		changePrescription.setBounds(350, 150, 200,30);
		add(textePrescription);
	    add(changePrescription);
		changePrescription.addActionListener(this);

       
        setLayout(null);
        setSize(600, 500);
        setVisible(true);
	}

	/*@Override
	public void itemStateChanged(ItemEvent e) {
		
		if (e.getStateChange() == ItemEvent.SELECTED) {
			System.out.println("index selectionn : "+selectIndex+ " patient" + patient.getLn()+"e : "+ e.getStateChange());
			//setSelectIndex(selectIndex);	
			try {
				textePrescription.setText( patient.consultations(idMedecin).get(ar.getSelectedIndex()).ordonnanceToString());
				  
			        changePrescription.addActionListener(this);

			} catch (SQLException e1) {
				e1.printStackTrace();
			}

		}
	}*/

	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		 if (e.getSource().equals(accueil)) {
	            this.dispose();
	            Application a = new Application();
	            a.setVisible(true);
	     }
		 if (e.getSource().equals(retour)) {
	            this.dispose();
	            (new MenuMedecin(idP,idMedecin)).setVisible(true);
	        }		
		else if (e.getSource().equals(changePrescription)) {
			try {
				patient.majOrdonnance(idMedecin,selectIndex,textePrescription.getText());
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		      
		}
	    
	}


	
}
