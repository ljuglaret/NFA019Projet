package nfa019Projet.Vue.Swing.Medecin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Modele.Appareil;
import nfa019Projet.Modele.Medicament;
import nfa019Projet.Modele.Prescription;
import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

public class AjoutPrescription extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private JButton retour ;
	private JButton ajoutM;
	private JTextField texteM = new JTextField(100);
	private JButton ajoutA;
	private JTextField texteA = new JTextField(100);
	private String idP;
	private String idMedecin;

	private  Swing ar;
	private int idCons;

	private int selectIndex  = 0 ;
	private JComboBox<String> ordoList;
	private PatientDAO patient;

	
	public AjoutPrescription(String idP,String idMedecin) {
		this.idP= idP;	
		this.idMedecin = idMedecin;
    	patient = new PatientDAO(SingleConnection.acces,idP);
    	
    	texteM.setBounds(30,150,150,50);
        add(texteM);
        ajoutM = new JButton("ajout Medicament");
        ajoutM.setBounds(200, 150, 150,50);
        ajoutM.addActionListener(this);
        add(ajoutM);
        
        texteA.setBounds(30,250,150,50);
        add(texteA);
        ajoutA = new JButton("ajout Appareil");
        ajoutA.setBounds(200, 250, 150,50);
        ajoutA.addActionListener(this);
        add(ajoutA);
    	
       ar = new Swing();
       int nbOrdonnances;
       try {
    	   nbOrdonnances = patient.consultations(idMedecin).size();
    	   String[] ordonnances = new String[nbOrdonnances];
    	   for (int i = 0 ; i < ordonnances.length ; i++) {	
				ordonnances[i]=	"Ordonnance du "+patient.consultations(idMedecin).get(i).getDate();
			}
    	   ar.comboList(ordonnances,30,50,200,60);

       } catch(SQLException e) {
    	   e.printStackTrace();
       }
      
	    ordoList = ar.getOrdoList();
		add(ar.getpanel());
        
        
        ar.accueilEtRetour();
        retour = ar.getretour();
        retour.addActionListener(this);
        add(retour);
        accueil = ar.getaccueil();
        accueil.addActionListener(this);
        add(accueil);
        setLayout(null);
        setSize(600, 500);
        setVisible(true);	
        setVisible(true);

	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		 if (e.getSource().equals(accueil)) {
	            this.dispose();
	            Application a = new Application();
	            a.setVisible(true);
	     }
		else if (e.getSource().equals(retour)) {
            this.dispose();
            (new MenuMedecin(idP,idMedecin)).setVisible(true);
        }	
		
		if (e.getSource().equals(ajoutM)) {
	      try {
			    Prescription prescriptionM = new Medicament(texteM.getText());
		
			    patient.addPrescription(idMedecin,ordoList.getSelectedIndex(), prescriptionM );
			} catch (SQLException er) {
				er.printStackTrace();
		}
		}
	      
	      if (e.getSource().equals(ajoutA)) {
		      try {
				    Prescription prescriptionA = new Appareil(texteA.getText());
					patient.addPrescription(idMedecin,ordoList.getSelectedIndex(),prescriptionA );
				} catch (SQLException er) {
					er.printStackTrace();
			}
	      }    
	}



}
