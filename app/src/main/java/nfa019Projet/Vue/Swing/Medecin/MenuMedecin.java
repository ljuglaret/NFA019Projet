package nfa019Projet.Vue.Swing.Medecin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;

import javax.swing.JButton;
import javax.swing.JFrame;

import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Modele.Consultation;
import nfa019Projet.Vue.Swing.Swing;

public class MenuMedecin extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private JButton retour ;
	private JButton consulter;
	private JButton creer;
	private JButton ajoutPrescription;
	private JButton modifOrd;
	private JButton supp;

	private String idP;
	private String idMedecin;
	public MenuMedecin(String idP,String idMedecin) {
		this.idP = idP;
		this.idMedecin = idMedecin;


		creer = new JButton("Ajouter Ordonnance");
		creer.setBounds(30, 50, 150,40);
		add(creer);
		creer.addActionListener(this);

		consulter = new JButton("Consulter");
		consulter.setBounds(30, 100, 150, 40);
		add(consulter);
		consulter.addActionListener(this);


		ajoutPrescription = new JButton("Ajouter Prescription");
		ajoutPrescription.setBounds(30,150, 150,40);
		add(ajoutPrescription);
		ajoutPrescription.addActionListener(this);



		modifOrd = new JButton("Modifier ordonnance");
		modifOrd.setBounds(30, 200, 150,40);
		add(modifOrd);
		modifOrd.addActionListener(this);


		supp = new JButton("Supprimer");
		supp.setBounds(30, 250, 150,40);
		add(supp);
		supp.addActionListener(this);

		Swing ar = new Swing();
		ar.accueilEtRetour();
		retour = ar.getretour();
		retour.addActionListener(this);
		add(retour);
		accueil = ar.getaccueil();
		accueil.addActionListener(this);
		add(accueil);

		setLayout(null);
		setSize(600, 500);
		setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e)  {

		System.out.println(idP + "idMedecin : " +  idMedecin);

		if (e.getSource().equals(creer)) {
			LocalDateTime dtm = LocalDateTime.now();  


			this.dispose();
			new PatientDAO(SingleConnection.acces,idP)
			.addOrdonnance(
					new Consultation(
							idP,idMedecin, java.sql.Date.valueOf(dtm.toLocalDate()), "D�tail : \n"
							)
					);

		}
		else if (e.getSource().equals(consulter)) {
			this.dispose();
			(new Consulter(idP,idMedecin)).setVisible(true);
		}

		else if (e.getSource().equals(ajoutPrescription)) {
			this.dispose();
			(new AjoutPrescription(idP,idMedecin)).setVisible(true);
		}	

		else if (e.getSource().equals(modifOrd)) {
			this.dispose();
			(new ModifierOrdonnance(idP,idMedecin)).setVisible(true);
		}	

		else if (e.getSource().equals(supp)) {
			this.dispose();
			(new SuppOrdonnance(idP,idMedecin)).setVisible(true);
		}	

		else if (e.getSource().equals(retour)) {
			this.dispose();
			InterfaceMedecin a = new InterfaceMedecin();
			a.setVisible(true);
		}	


	}

}
