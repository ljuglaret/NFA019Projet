package nfa019Projet.Vue.Swing.Medecin;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import nfa019Projet.CRUD.EmployeDAO;
import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class Consulter  extends JFrame implements ActionListener, ItemListener {

	
	private static final long serialVersionUID = 1L;
	private int selectIndex  = 0 ;
	private JButton accueil;
	private JButton retour;
	private JButton impressionButton;
	private String idP;
	private JComboBox<String> ordoList;
	private PatientDAO patient;
private Swing ar;
	private String idMedecin;
	
	
	
	
	public Consulter(String idP,String idMedecin) {
		this.idP= idP.strip();	
		this.idMedecin = idMedecin.strip();
    	patient = new PatientDAO(SingleConnection.acces,this.idP);

    	
    	 ar = new Swing();
    	 ar.accueilEtRetour();
         retour = ar.getretour();
         retour.addActionListener(this);
         add(retour);
         accueil = ar.getaccueil();
         accueil.addActionListener(this);
         add(accueil);
         int nbOrdonnances;
         try {
      	   nbOrdonnances = patient.consultations(idMedecin).size();
      	   String[] ordonnances = new String[nbOrdonnances];
      	   for (int i = 0 ; i < ordonnances.length ; i++) {	
  				ordonnances[i]=	"Ordonnance du "+patient.consultations(idMedecin).get(i).getDate();
  			}
      	   ar.comboList(ordonnances,30,50,200,60);

         } catch(SQLException e) {
      	   e.printStackTrace();
         } 	    ordoList =ar.getOrdoList();
 		add(ar.getpanel());
	    
		impressionButton = new JButton("impression");
		impressionButton.setBounds(30, 150, 300, 50);
		impressionButton.setFont(new Font("Serif", Font.PLAIN, 24));
        add(impressionButton);
        impressionButton.addActionListener(this);
     
	      
        setLayout(null);
        setSize(600, 500);
        setVisible(true);		
        
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource().equals(ordoList)) {
			System.out.println("index selectionn� : "+selectIndex);
			this.selectIndex = ar.getSelectedIndex();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		 if (e.getSource().equals(accueil)) {
	            this.dispose();
	            Application a = new Application();
	            a.setVisible(true);
	     }
		 else if (e.getSource().equals(retour)) {
	            this.dispose();
	           (new MenuMedecin( idP,idMedecin)).setVisible(true);
	     }	
		 else if (e.getSource().equals(impressionButton)) {
				System.out.println("imprimer : cons numero" + ar.getSelectedIndex());

		      Document document = new Document();
			 try
		      { 
				 EmployeDAO employe = new EmployeDAO( patient.getFileConf(), idMedecin);

		         PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Cons.pdf"));
		         document.open();
		         document.add(new Paragraph("Consultation �mise par " + 
							employe.getNomEmploye()+ " "+
							employe.getPreNomEmploye()+"\n"));
		         try {
					document.add(new Paragraph(
					 		"\nConsultation du " + patient.consultations(idMedecin).get(ar.getSelectedIndex()).getDate() + " pour "
							+"\nPr�nom " + patient.getFn()
					 		+ "\nNom " + patient.getLn() )
					);
					document.add(new Paragraph(
					 		"\nPrescription(s)"
							+"\n"+patient.consultations(idMedecin).get(ar.getSelectedIndex()).ordonnanceToString()
							)
					);

				
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		         document.close();
		         writer.close();
		      } catch (DocumentException e1)
		      {
		         e1.printStackTrace();
		      } catch (FileNotFoundException e2)
		      {
		         e2.printStackTrace();
		      }
			 

		 }
	
	}
	
}
