package nfa019Projet.Vue.Swing.Medecin;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

public class SuppOrdonnance extends JFrame implements ActionListener, ItemListener {

	
	private static final long serialVersionUID = 1L;
	private int selectIndex  = 0 ;
	private JButton accueil;
	private JButton retour;
	private JButton supressionButton;
	private String idP;
	private JComboBox<String> ordoList;
	private PatientDAO patient;

	private String idMedecin;
	
	
	
	
	public SuppOrdonnance(String idP,String idMedecin) {
		this.idP= idP;	
		this.idMedecin = idMedecin;
    	patient = new PatientDAO(SingleConnection.acces,idP);

    	
    	Swing ar = new Swing();
    	 ar.accueilEtRetour();
         retour = ar.getretour();
         retour.addActionListener(this);
         add(retour);
         accueil = ar.getaccueil();
         accueil.addActionListener(this);
         add(accueil);
         int nbOrdonnances;
         try {
      	   nbOrdonnances = patient.consultations(idMedecin).size();
      	   String[] ordonnances = new String[nbOrdonnances];
      	   for (int i = 0 ; i < ordonnances.length ; i++) {	
  				ordonnances[i]=	"Ordonnance du "+patient.consultations(idMedecin).get(i).getDate();
  			}
      	   ar.comboList(ordonnances,30,50,200,60);

         } catch(SQLException e) {
      	   e.printStackTrace();
         } 	    ordoList =ar.getOrdoList();
 		add(ar.getpanel());
	    
		supressionButton = new JButton("Suprimer");
		supressionButton.setBounds(30, 150, 300, 50);
		supressionButton.setFont(new Font("Serif", Font.PLAIN, 24));
        add(supressionButton);
        supressionButton.addActionListener(this);
     
	      
        setLayout(null);
        setSize(600, 500);
        setVisible(true);		
        
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource().equals(ordoList)) {
			System.out.println("index selectionné : "+selectIndex);
			this.selectIndex = ordoList.getSelectedIndex(); 
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		 if (e.getSource().equals(accueil)) {
	            this.dispose();
	            Application a = new Application();
	            a.setVisible(true);
	     }
		 else if (e.getSource().equals(retour)) {
	            this.dispose();
	           (new MenuMedecin( idP,idMedecin)).setVisible(true);
	     }	
		 else if (e.getSource().equals(supressionButton)) {
	
			 

		 }
	
	}
	
}
