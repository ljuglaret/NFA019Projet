package nfa019Projet.Vue.Swing.Medecin;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import nfa019Projet.CRUD.EmployeDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

public class ChoisirPatient extends JFrame implements ActionListener, ItemListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private Swing ar = new Swing();
	private String idMedecin;
	String idPatient;
	private JButton retour ;

	private EmployeDAO emp;
	private JComboBox<String> ordoList;
	private int selectIndex  = 0 ;
	private String[] patients;

	public ChoisirPatient(String idM ) {
		this.idMedecin = idM;
		ar = new Swing();
		emp = new EmployeDAO(SingleConnection.acces,idMedecin);
		patients = new String[emp.getPatients().size() + 1 ];
		patients[0] = "------Faites un choix------";
		for (int i = 1 ; i < patients.length ; i++) {	
			patients[i]=	emp.getPatients().get(i-1).getId();

		}
		ar.comboList(patients,150,30,200,60);


		ordoList =ar.getOrdoList();
        ordoList.addItemListener(this);
		add(ar.getpanel());



		ar.accueilEtRetour();
		retour = ar.getretour();
		retour.addActionListener(this);
		add(retour);
		accueil = ar.getaccueil();
		accueil.addActionListener(this);
		add(accueil);
		setLayout(null);
		setSize(600, 500);
		setVisible(true);	


	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(accueil)) {
			this.dispose();
			Application a = new Application();
			a.setVisible(true);
		}	
		else if (e.getSource().equals(retour)) {
			this.dispose();
			(new InterfaceMedecin()).setVisible(true);
		}	
	}

	@Override
	public void itemStateChanged(ItemEvent e) {

		if (e.getSource().equals(ordoList)) {
			System.out.println("index selectionné : "+selectIndex);
			this.selectIndex = ordoList.getSelectedIndex(); 
			idPatient = patients[this.selectIndex];
			System.out.println(idPatient);
			this.dispose();
			(new MenuMedecin(idPatient,idMedecin)).setVisible(true);	

		}
	}


}

