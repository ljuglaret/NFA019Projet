package nfa019Projet.Vue.Swing.Agent;

import javax.swing.*;

import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

import java.awt.*;
import java.awt.event.*;


class Consulter extends JFrame implements ActionListener {

 
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private JButton retour;
	public Consulter (){
	
	  Swing ar = new Swing();
      ar.accueilEtRetour();
      retour = ar.getretour();
      retour.addActionListener(this);
      add(retour);
      accueil = ar.getaccueil();
      accueil.addActionListener(this);
      add(accueil);
	}

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(retour)) {
            this.dispose();
            (new AgentAdmVue()).setVisible(true);
        }

	
    }
	
}