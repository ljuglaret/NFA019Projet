package nfa019Projet.Vue.Swing.Agent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Vue.Swing.Swing;

public class ModifierPatient extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton accueil;
	private JButton retour ;
	private JButton recherchePatientParId;
	private JTextField textRechercheId = new JTextField(100);
	private JButton changeIdPatient;
	private JTextField texteId = new JTextField(100);
	private JButton changeNomPatient;
	private JTextField texteNom = new JTextField(100);
	private JTextField textePrenom = new JTextField(100);
	private JButton changePrenomPatient;
	private String idP;

	PatientDAO patient;

	public ModifierPatient() {

		//ajouter checkbox pour choisir un patient

  
		textRechercheId.setBounds(30,5,30,30);
		add(textRechercheId);
		recherchePatientParId = new JButton("recherche id");
		recherchePatientParId.setBounds(250, 5, 200,30);
		add(recherchePatientParId);
		recherchePatientParId.addActionListener(this);
    	

        texteId.setBounds(30,50,300,80);
        changeIdPatient = new JButton("changer id");
        changeIdPatient.setBounds(350, 50, 100,50);
        texteNom.setBounds(30,150,300,80);
        changeNomPatient = new JButton("changer nom");
        changeNomPatient.setBounds(350, 150, 150,50);
        textePrenom.setBounds(30,250,300,80);
        changePrenomPatient = new JButton("changer prenom");
        changePrenomPatient.setBounds(350, 250, 150,50);
        

        Swing ar = new Swing();
        ar.accueilEtRetour();
        retour = ar.getretour();
        retour.addActionListener(this);
        add(retour);
        accueil = ar.getaccueil();
        accueil.addActionListener(this);
        add(accueil);
        setLayout(null);
        setSize(600, 500);
        setVisible(true);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(retour)) {
            this.dispose();
            (new AgentAdmVue()).setVisible(true);
        }	
		else if (e.getSource().equals(recherchePatientParId)) {
			this.idP = textRechercheId.getText();
	    	patient = new PatientDAO(SingleConnection.acces,idP);

         
			texteId.setText(String.valueOf(idP));
	        add(texteId);
	        add(changeIdPatient);
	        changeIdPatient.addActionListener(this);
	      
			
				texteNom.setText(patient.getLn());
		        add(texteNom);
		        add(changeNomPatient);
		        changeNomPatient.addActionListener(this);
		
				textePrenom.setText(patient.getFn());
		        add(textePrenom);
		        add(changePrenomPatient);
		        changePrenomPatient.addActionListener(this);
			

		}
		else if (e.getSource().equals(changeIdPatient)) {
			patient.setPatientID(texteId.getText());
		}
		else if (e.getSource().equals(changeNomPatient)) {
			patient.setPatientName(texteNom.getText());
		}
		else if (e.getSource().equals(changePrenomPatient)) {
			patient.setPatientFirstName(textePrenom.getText());
		}
	    
	}
}
