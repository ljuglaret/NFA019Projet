package nfa019Projet.Vue.Swing.Agent;

import javax.swing.*;

import nfa019Projet.CRUD.PatientDAO;
import nfa019Projet.CRUD.SingleConnection;
import nfa019Projet.Vue.Swing.Application;
import nfa019Projet.Vue.Swing.Swing;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;


class Ajouter extends JFrame implements ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton jb1;
    JLabel l1;
    JButton accueil;
	private JFrame frame;
	private JTextField textFieldId;
	private JTextField textFieldAdresse;
	private JTextField textNom;
	private JTextField textFieldPrenom;
	JButton ajouter;
	private JButton btnClear;
	private JButton btnSubmit;
	private JButton btnRetour;
	private String idPatient;
	private PatientDAO patient;

    Ajouter() {

        frame = new JFrame();
		frame.setBounds(100, 100, 730, 489);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblId = new JLabel("id");
		lblId.setBounds(70, 110, 50, 20);
		frame.getContentPane().add(lblId);
        add(lblId);
		textFieldId = new JTextField();
		textFieldId.setBounds(130, 110, 200, 20);
		frame.getContentPane().add(textFieldId);
		textFieldId.setColumns(10);
		
		JLabel lblAdresse = new JLabel("Adresse");
		lblAdresse.setBounds(70, 140, 50, 20);
		frame.getContentPane().add(lblAdresse);
        add(lblAdresse);
		textFieldAdresse = new JTextField();
		textFieldAdresse.setBounds(130, 140, 200, 20);
		frame.getContentPane().add(textFieldAdresse);
		textFieldAdresse.setColumns(10);
		
		JLabel lblNom = new JLabel("Nom");
		lblNom.setBounds(70, 170, 50, 20);
		frame.getContentPane().add(lblNom);
        add(lblNom);
        textNom = new JTextField();
		textNom.setBounds(130, 170, 200, 20);
		frame.getContentPane().add(textNom);
		textNom.setColumns(10);
		
		JLabel lblPrenomId = new JLabel("Prenom");
		lblPrenomId.setBounds(70, 200, 50, 20);
		frame.getContentPane().add(lblPrenomId);
		add(lblPrenomId);
		textFieldPrenom = new JTextField();
		textFieldPrenom.setBounds(130, 200, 200, 20);
		frame.getContentPane().add(textFieldPrenom);
		textFieldPrenom.setColumns(10);
	

		add(textFieldId);
        add(textFieldAdresse);
        add(textNom);
        add(textFieldPrenom);
	
		
		btnSubmit = new JButton("Enregistrer");		
		btnSubmit.setBackground(Color.BLUE);
		btnSubmit.setForeground(Color.MAGENTA);
		btnSubmit.setBounds(420, 387, 89, 23);
		add(btnSubmit);
		btnSubmit.addActionListener(this);
		

		btnClear = new JButton("Effacer");
		btnClear.setBounds(312, 387, 89, 23);
		frame.getContentPane().add(btnClear);
		btnClear.addActionListener(this);
		add(btnClear);
		
		
		Swing ar = new Swing();
        ar.accueilEtRetour();
        btnRetour = ar.getretour();
        btnRetour.addActionListener(this);
        add(btnRetour);
        accueil = ar.getaccueil();
        accueil.addActionListener(this);
        add(accueil);

        setLayout(null);
        setSize(600, 500);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(accueil)) {
            this.dispose();
            Application a = new Application();
            a.setVisible(true);
        }
        
        else if (e.getSource().equals(btnRetour)) {
        	this.dispose();
        	(new AgentAdmVue()).setVisible(true);
        } 

        else if(e.getSource().equals(btnClear)){
        	textFieldId.setText(null);
			textNom.setText(null);
			textFieldPrenom.setText(null);
			textFieldAdresse.setText(null);
        }
        else if (e.getSource().equals(btnSubmit)) {
        	if(textFieldAdresse.getText().isEmpty()||
					(textNom.getText().isEmpty())||
					(textFieldPrenom.getText().isEmpty()))
					JOptionPane.showMessageDialog(null, "Data Missing");
			else{
	            idPatient =textFieldId.getText();
	            patient = new PatientDAO(SingleConnection.acces,idPatient);
	            try {
					patient.createPatient(idPatient,textFieldPrenom.getText(),textNom.getText(),textFieldPrenom.getText());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
        }
    }

}