package nfa019Projet.Vue.Swing.Agent;

import javax.imageio.ImageIO;
import javax.swing.*;

import nfa019Projet.Vue.Swing.Application;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.InputStream;


public class AgentAdmVue extends JFrame implements ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton jb1;
	private JFrame frame;
	private JButton accueil;
	private JButton modifier;
	JButton ajouter;
    public AgentAdmVue() {

        frame = new JFrame();
		frame.setBounds(100, 100, 730, 489);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);


		ajouter = new JButton("ajouter");
		ajouter.setBounds(80,0, 150, 50);
		frame.getContentPane().add(ajouter);
		add(ajouter);
		ajouter.addActionListener(this);

		 modifier = new JButton("modifier");
		modifier.setBounds(80, 60, 150, 50);
		frame.getContentPane().add(modifier);
		add(modifier);
		modifier.addActionListener(this);

		JButton supprimer = new JButton("supprimer");
		supprimer.setBounds(80, 120, 150, 50);
		frame.getContentPane().add(supprimer);
		add(supprimer);	
	
		
		JButton btnSubmit = new JButton("submit");
		add(btnSubmit);
		
		btnSubmit.setBackground(Color.BLUE);
		btnSubmit.setForeground(Color.MAGENTA);
		btnSubmit.setBounds(65, 387, 89, 23);
		frame.getContentPane().add(btnSubmit);
		 ClassLoader classloader = Thread.currentThread().getContextClassLoader();

	        InputStream isS = classloader.getResourceAsStream("img/home.png");
	       
        Image imageAc;
		try {
			imageAc = ImageIO.read(isS);
			accueil = new JButton("agent",new ImageIcon(imageAc));
			accueil.setBounds(200,200, 80, 80);
			add(accueil);
		    accueil.addActionListener(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        setLayout(null);
        setSize(600, 500);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(accueil)) {
            this.dispose();
            Application a= new Application();
            a.setVisible(true);
        }

        else if (e.getSource().equals(ajouter)) {
            this.dispose();
            (new Ajouter()).setVisible(true);
		}
		
        else if (e.getSource().equals(modifier)) {
            this.dispose();
            (new ModifierPatient()).setVisible(true);
		}
    }

}