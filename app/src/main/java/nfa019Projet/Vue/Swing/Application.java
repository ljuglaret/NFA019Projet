package nfa019Projet.Vue.Swing;

import javax.imageio.ImageIO;
import javax.swing.*;

import nfa019Projet.App;
import nfa019Projet.Vue.Swing.Agent.AgentAdmVue;
import nfa019Projet.Vue.Swing.Technicien.InterfaceTechnicien;

import java.awt.event.*;
import java.io.*;
import java.awt.*;
import nfa019Projet.Vue.Swing.Medecin.InterfaceMedecin;
public class Application extends JFrame implements ActionListener {

    JButton interfaceAgentAdm;
    JButton interfaceMedecin;
    JButton interfaceTechnicien;
    JLabel l1;

    public Application(){

        l1 = new JLabel("Bienvenue dans l'application");
        l1.setBounds(SwingConstants.CENTER, 50, 300, 50);
        l1.setFont(new Font("Serif", Font.PLAIN, 24));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER));

        add(l1);

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

        /* Icone Agent Administratif*/
        try{
            InputStream isS = classloader.getResourceAsStream("img/secretariat.jpg");
            Image imageAA = ImageIO.read(isS);
            interfaceAgentAdm = new JButton("agent",new ImageIcon(imageAA));
            interfaceAgentAdm.setBounds(75, 100, 150, 150);
            add(interfaceAgentAdm);
            interfaceAgentAdm.addActionListener(this);
        }        
        catch(IOException e){
            e.printStackTrace();
        }
        

        /* Icone Medecin */
        try{

            InputStream isM = classloader.getResourceAsStream("img/medical.jpg");
            Image imageM = ImageIO.read(isM);
            interfaceMedecin = new JButton("medecin",new ImageIcon(imageM));
            interfaceMedecin.setBounds(375, 100, 150, 150);
            add(interfaceMedecin);
            interfaceMedecin.addActionListener(this);
        }
        catch(IOException e){
            e.printStackTrace();
        }

        /* Icone Technicien */
        try{

            InputStream isT = classloader.getResourceAsStream("img/technicien.jpg");
            Image imageT = ImageIO.read(isT);
            interfaceTechnicien = new JButton("technicien",new ImageIcon(imageT));
            interfaceTechnicien.setBounds(200, 300, 150, 150);
            add(interfaceTechnicien);
            interfaceTechnicien.addActionListener(this);
           
        }
        catch(IOException e){
            e.printStackTrace();
        }
 
        setLayout(null);
        setSize(600, 600);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(interfaceAgentAdm)) {
            this.dispose();
            AgentAdmVue aa= new AgentAdmVue();
            aa.setVisible(true);
        }
        if (e.getSource().equals(interfaceMedecin)) {
            this.dispose();
            InterfaceMedecin m = new InterfaceMedecin();
            m.setVisible(true);
        }
        if (e.getSource().equals(interfaceTechnicien)) {
            this.dispose();
            InterfaceTechnicien t = new InterfaceTechnicien();
            t.setVisible(true);
        }
    }

}

