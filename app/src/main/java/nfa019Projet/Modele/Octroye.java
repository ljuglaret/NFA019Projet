package nfa019Projet.Modele;

public class Octroye implements StatutAppareil  {	

    public void accept(VisiteurStatutAppareil v) {
        v.visit(this);
    }

    public String getStatut(){
        return "octroye";
    }
}