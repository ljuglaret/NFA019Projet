package nfa019Projet.Modele;

import java.util.HashSet;

import nfa019Projet.Modele.Roles.AgentAdministration;
import nfa019Projet.Modele.Roles.Medecin;
import nfa019Projet.Modele.Roles.Patient;
import nfa019Projet.Modele.Roles.Technicien;

public abstract class Hopital {

    private HashSet<Patient> patients;
    
    public Hopital(){
        patients = new HashSet<>();
    }

    public HashSet<Patient> getPatients() {
        return patients;
    }

    public abstract void ajoutMedecin(Medecin medecin);

    public abstract void ajoutAgentAdm(AgentAdministration agent);

    public abstract void ajoutTechnicien(Technicien technicien);
}
