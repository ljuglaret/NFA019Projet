package nfa019Projet.Modele;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


public class Consultation {
    private List<Prescription> ordonnance;

    private String idPatient;
    private String idMedecin;
    private java.sql.Date date;
    
   public Consultation( String idPatient,String idMedecin, java.sql.Date date){
    	this.idPatient = idPatient.strip();
    	this.idMedecin = idMedecin.strip();
    	this.date = date;
    	ordonnance = new ArrayList<>();
    }
    
    public Consultation( String idPatient,String idMedecin, java.sql.Date date, String detail){
    	this.idPatient = idPatient;
    	this.idMedecin = idMedecin;
    	this.date = date;
    	ordonnance = new ArrayList<>();
    	for (String d : detail.split("\n")) {
        	ordonnance.add(toPrescription(d));
    	}
    }
    
    public String getIdMedecin() {
    	return idMedecin;
    }
    
    public Prescription toPrescription (String detail) {
    	if(detail.split(" : ")[0].equals("medicament")) {
    		return new Medicament(detail.split(" : ")[1]);
    	}
    	if(detail.split(" : ")[0].equals("appareil")) {
    		return new Appareil(detail.split(" : ")[1]);
    	}
    	else return null;
    }
 
   
    public void ajout(Prescription prescription){
        ordonnance.add(prescription);
    }

    public void ajoutAppareil (Appareil appareil){
        ordonnance.add(appareil);
        appareil.setStatut(new Instance());
    }

    public void octroyerAppareil(Appareil appareil){
        ordonnance.remove(appareil);
        appareil.setStatut(new Octroye());
        ordonnance.add(appareil);
    }

    public List<Prescription> getOrdonnance() {
        return ordonnance;
    }
    
    public String ordonnanceToString() {
    	String detail = "";
    	for (Prescription d : ordonnance) {
    		detail = detail + d.ptoString()+ "\n";
    	}
    	return detail;
    }
    
    
    
    public Date getDate() {
    	return date;
    }

    public void afficherOrdonnance(){
        System.out.println(this.idPatient);

        for (Prescription prescription : ordonnance){
           prescription.ptoString();
        }
    }
}
