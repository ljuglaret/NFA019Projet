package nfa019Projet.Modele;


public class Appareil extends Prescription  {	
    private StatutAppareil statut;
    private String nomAppareil;
    public void accept(VisiteurPrescription v) {
        v.visit(this);
    }
    public Appareil(String  nomAppareil){
        this.nomAppareil = nomAppareil;
        this.statut = new Libre();
    }

    @Override
    public String getNom() {
        // TODO Auto-generated method stub
        return  this.nomAppareil;
    }
public void setStatut(StatutAppareil statut) {
    this.statut = statut;
}
    public StatutAppareil getStatutAppareil(){
       return statut;
    }

    public String getNomAppareil() {
        return nomAppareil;
    }
    public String ptoString(){
    	return "appareil" + " : " + nomAppareil;
    }
   
}