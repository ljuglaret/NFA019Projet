package nfa019Projet.Modele;

interface VisiteurPrescription {
   void visit(Medicament objet);
   void visit(Appareil objet);
   void visit(Soin objet);
}



