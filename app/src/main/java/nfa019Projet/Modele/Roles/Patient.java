package nfa019Projet.Modele.Roles;


import java.sql.Date;


public class Patient {
	private String id;
	private String adresse;
	private String ln;
	private String fn;
	private String secu;
	private Date dateCreation;
    

	  public Patient(String id , String adresse, String ln, String fn, String secu, Date dateCreation) {
		  this.id = id;
		  this.adresse = adresse;
		  this.ln = ln;
		  this.fn = fn;
		  this.secu = secu;
		  this.dateCreation = dateCreation;
	  }
	  public Patient(String id) {
		  this.id = id;
	  }



    public String getId() {
		return id;
	}
	

	public String getadresse() {
		return adresse;
	}
	

	public  String getLn() {
		return ln;
	}
	

	public String getFn() {
		return fn;
	}

	public String getSecu() {
		return secu;
	}

	public Date getDateCreation() {
		return dateCreation;
	}
	public void setId(String id) {
		this.id = id;
	}
	

	public void setAdresse(String s) {
		this.adresse = s;
	}
	

	public void setLn(String ln) {
		this.ln = ln;
	}
	

	public void setFn(String fn) {
		this.fn = fn;
	}

	public void setSecu(String secu) {
		this.secu = secu;
	}
	
	public String toString() {
		return "patient numero " + id + "; nom : " + ln + " ; prenom : " + fn ;
	}
	
	 @Override
	  public boolean equals(Object o) {
	     if (!(o instanceof Patient)){ 
	       return false;
	     }
	     return ((Patient) o).getId().equals(this.getId());
	  }


}




	
	

