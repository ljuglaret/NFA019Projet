package nfa019Projet.Modele.Roles;

import nfa019Projet.CRUD.Employes;
import nfa019Projet.Modele.Appareil;
import nfa019Projet.Modele.Consultation;

public abstract class Medecin extends Employes {
    public void accept(VisiteurPersonnel v) {
        v.visit(this);
    }

    public Medecin(String idM,String lastname, String firstname,String city, double salary){
        super(idM, lastname, firstname,city,  salary);
    }
    public Medecin(String idM){
        super(idM);
    }


    public abstract void addOrdonnance(Consultation c);
	
    public abstract void enleveConsultation(Consultation cons);
	    
    public void changerStatutAppareil(Appareil app, Consultation cons){
        cons.ajoutAppareil(app);
    }
    
	

   
}
