package nfa019Projet.Modele.Roles;

import nfa019Projet.CRUD.Employes;

public abstract class AgentAdministration extends Employes {

    public AgentAdministration(String idA, String lastname, String firstname, String city, double salary){
        super(idA, lastname, firstname, city,  salary);
    }
   
    public AgentAdministration(String idA){
        super(idA);
    }
   

    public void accept(VisiteurPersonnel v) {
        v.visit(this);
    }

   /**
    * Si le patient n’existe pas dans la liste des 
patients, l’agent doit alors pouvoir le rajouter via un formulaire de rajout. Il doit aussi pouvoir 
modifier les détails d’un patient, ou le supprimer
    * @param patient
    * @param consultations
    */
    
    public abstract void ajoutPatient(Patient patient);
    public abstract void modifieNom(Patient patient , String nouveauNom);
    public abstract void modifiePrenom(Patient patient , String nouveauPrenom);
    public abstract void supprimePatient(Patient patient);
}
