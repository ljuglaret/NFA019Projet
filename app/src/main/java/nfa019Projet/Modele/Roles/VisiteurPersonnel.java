package nfa019Projet.Modele.Roles;

interface VisiteurPersonnel {
    void visit(Medecin objet);
    void visit(Technicien objet);
    void visit (AgentAdministration objet);
 }
 
 