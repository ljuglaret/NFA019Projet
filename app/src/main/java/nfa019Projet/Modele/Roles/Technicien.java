package nfa019Projet.Modele.Roles;

import nfa019Projet.CRUD.Employes;
import nfa019Projet.Modele.*;


public class Technicien extends Employes {
    public void accept(VisiteurPersonnel v) {
        v.visit(this);
    }


    public Technicien(String idT,String lastname, String firstname, String city,double salary){
        super(idT, lastname, firstname, city, salary);
    }
    
    public Technicien(String idT) {
    	super(idT);
    }

   
    public void changerStatutAppareil(Appareil app, Consultation cons){
        cons.octroyerAppareil(app);
    }

}
