package nfa019Projet.Modele;

public class Medicament extends Prescription  {

    private String nomMedicament;

    public Medicament(String nomMediacament){
        this.nomMedicament = nomMediacament;
    }
    
    public void accept(VisiteurPrescription v) {
        v.visit(this);
    }

    public String ptoString(){
    	return "medicament" + " : " + nomMedicament;
    }
    public String getNom() {
        return nomMedicament;
    }
}