package nfa019Projet.Modele;

public class Libre implements StatutAppareil  {	

    public void accept(VisiteurStatutAppareil v) {
        v.visit(this);
    }

    public String getStatut(){
        return "libre";
    }
}