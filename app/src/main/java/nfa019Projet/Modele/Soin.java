package nfa019Projet.Modele;

public class Soin extends Prescription {	

    private String nomSoin;

    public Soin(String nomSoin){
        this.nomSoin = nomSoin;
    }
    
    public void accept(VisiteurPrescription v) {
        v.visit(this);
    }

    public String getNom() {
        return nomSoin;
    }
    
    public String ptoString(){
    	return "soin" + " : " + nomSoin;
    }
}