package nfa019Projet.CRUD;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public final class SingleConnection {

    private static Connection connection;
    private String url;
    private String user;
    private String pwd;
public static String acces;
    private SingleConnection(String fileConf) throws SQLException {
    	acces = fileConf;
    	try {
    		Class.forName("com.mysql.cj.jdbc.Driver"); 
    		try (InputStream input = new FileInputStream(fileConf)) {
                Properties prop = new Properties();
                prop.load(input);
                this.url = prop.getProperty("url");
	            this.user = prop.getProperty("user");
	            this.pwd =   prop.getProperty("pwd");
                
             
                connection = DriverManager.getConnection(url, user, pwd);

    			DriverManager.getConnection(url ,user,pwd);   
    		
    			System.out.println(" single connection Connection Established for " + user + " with "+pwd);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
    	}
    	catch(ClassNotFoundException | SQLException e){
    		System.err.println(e);
    	}
    }

   

    public static Connection getInstance(String fileConf) throws SQLException {
        if (connection == null) {
           new SingleConnection(fileConf);
        }
        return connection;
    }
    
  
}


