package nfa019Projet.CRUD;


/*
 * : attribut id et salary de type int et attributs lastname,
firstname, city de type String. Ajoutez un constructeur, des accesseurs et une m�thode
toString.
 */
public class Employes {

	private String id;
	private double salary;
	String ln;
	String fn;
	String city;    
	
	public Employes() {}
	
	
	
    public Employes(String id) {this.id = id;}
	public Employes(String id, String ln , String fn , String city,double salary) {
		this.id = id;
		this.salary = salary;
		this.ln = ln;
		this.fn = fn;
		this.city= city;
	}
	
	public String getId() {
		return id;
	}
	

	public double getSalary() {
		return salary;
	}
	

	public String getLn() {
		return ln;
	}
	

	public String getFn() {
		return fn;
	}
	
	public String getCity() {
		return city;
	}

	
	public void setId(String id) {
		this.id = id;
	}
	

	public void setSalary(int s) {
		this.salary = s;
	}
	

	public void setLn(String ln) {
		this.ln = ln;
	}
	

	public void setFn(String fn) {
		this.fn = fn;
	}
	
	public void setCity(String c) {
		this.city = c;
	}
	
	public String toString() {
		return "employe numero " + id + "; nom : " + ln + " ; prenom : " + fn 
				+ "; ville : " + city + "; salaire : " + salary;
	}
}
