package nfa019Projet.CRUD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nfa019Projet.Modele.Roles.Patient;
import nfa019Projet.Modele.Consultation;


public class EmployeDAO {
	Connection conn = null;
	private String idE;
	//Un constructeur permettant d��tablir la connexion avec la base de donn�es en
	//passant par la classe SingleConnection

	public EmployeDAO(String fileConf, String idE) {		
		 try {
			this.conn = SingleConnection.getInstance(fileConf);
			this.idE = idE.strip();

		 } catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * 
	 * @param id
	 * @return retourner l�employ� ayant l'id : id
	 * @throws SQLException
	 */
	public  Employes findByID(String id) throws SQLException {
	        String query = "select * from employes where id= ?";
	        PreparedStatement ps = conn.prepareStatement(query);
	        ps.setString(1, id);
	       Employes emp = new Employes();
	        ResultSet rs = ps.executeQuery();
	        boolean check = false;
	        while (rs.next()) {
	            check = true;
	            emp.setId(rs.getString("id"));
	            emp.setSalary(rs.getInt("salary"));
	            emp.setFn(rs.getString("firstname"));
	            emp.setLn(rs.getString("lastname"));
	            emp.setCity(rs.getString("city"));
	            System.out.println(idE);
	        }
	        if (check == true) {
	            return emp;
	        }
	        else
	            return null;
	    }



	
	/**
	 * @param city
	 * @return employ�s de city
	 * @throws SQLException
	 */
	public List<Employes> findAll(String city) throws SQLException {
	        String query = "select * from employes where city= ?";
	        PreparedStatement ps = conn.prepareStatement(query);
	        ps.setString(1,city);
	        ResultSet rs = ps.executeQuery();
	        List<Employes> ls = new ArrayList<Employes>();
	  
	        while (rs.next()) {
	            Employes emp = new Employes();
	            emp.setId(rs.getString("id"));
	            emp.setSalary(rs.getInt("salary"));
	            emp.setFn(rs.getString("firstname"));
	            emp.setLn(rs.getString("lastname"));
	            emp.setCity(rs.getString("city"));
	            ls.add(emp);
	        }
	        return ls;
	    }

	
/**
 * 	
 * @return tous les employes
 * @throws SQLException
 */
	public List<Employes> getEmployees() throws SQLException {
	        String query = "select * from employes";
	        PreparedStatement ps = conn.prepareStatement(query);
	        ResultSet rs = ps.executeQuery();
	        List<Employes> ls = new ArrayList<Employes>();
	  
	        while (rs.next()) {
	            Employes emp = new Employes();
	            emp.setId(rs.getString("id"));
	            emp.setSalary(rs.getInt("salary"));
	            emp.setFn(rs.getString("firstname"));
	            emp.setLn(rs.getString("lastname"));
	            emp.setCity(rs.getString("city"));
	            ls.add(emp);
	        }
	        return ls;
	    }
	

	public  Employes getEmployee() throws SQLException {
	        String query = "select * from employes where id= ?";
	        PreparedStatement ps = conn.prepareStatement(query);
	        ps.setString(1, idE);
	       Employes emp = new Employes();
	        ResultSet rs = ps.executeQuery();
	        boolean check = false;
	        while (rs.next()) {
	            check = true;
	            emp.setId(rs.getString("id"));
	            emp.setSalary(rs.getInt("salary"));
	            emp.setFn(rs.getString("firstname"));
	            emp.setLn(rs.getString("lastname"));
	            emp.setCity(rs.getString("city"));
	            System.out.println(idE);
	        }
	        if (check == true) {
	            return emp;
	        }
	        else
	            return null;
	    }
	
	/**
	 * 
	 * @return nom employe
	 */
	public String getNomEmploye() {
		try {
			return getEmployee().getLn();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @return prenom employe
	 */
	public String getPreNomEmploye() {
		try {
			return getEmployee().getFn();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * permet d�ins�rer dans la base l�employ� emp
	 * @param emp
	 * @return
	 * @throws SQLException
	 */
	public int insert(Employes emp) throws SQLException {
	  
	        String query = "insert into employes(id, "
	        								+ "lastname"
	        								+ "firstname"
	        								+ "city"
	        								+ "salary"
	        								+ ") VALUES (?, ?, ? , ? , ?)";
	        PreparedStatement ps = conn.prepareStatement(query);
	        ps.setString(1, emp.getId());
	        ps.setString(2, emp.getLn());
	        ps.setString(3, emp.getFn());
	        ps.setString(4, emp.getCity());
	        ps.setDouble(5, emp.getSalary());
	        
	        int n = ps.executeUpdate();
	        return n;
	    }
	
	/**
	 * permet de mettre � jour l�employ� emp
	 * @param emp
	 * @throws SQLException
	 */
	public void update(Employes emp) throws SQLException {
	  
	        String query
	            = "update employee set id=?, "
	              + " lastname=?,"
	              + " firstname=?, "
	              + "city=? "
	              + "salar=? "
	              + " where id = ?";
	        PreparedStatement ps   = conn.prepareStatement(query);
	        ps.setString(2, emp.getLn());
	        ps.setString(3, emp.getFn());
	        ps.setString(4, emp.getCity());
	        ps.setDouble(5, emp.getSalary());
	        ps.executeUpdate();
	        ps.close();
	    }

	public List<Patient> getPatients() {
		List<Patient> patients = new ArrayList<Patient>();
		String query = "select * from ordonnance where idMedecin=?";
        PreparedStatement ps;
		try {
			ps = conn.prepareStatement(query);
	        ps.setString(1,idE);
	        ResultSet rs = ps.executeQuery();
	        while(rs.next()) {
	        	Patient pat = new Patient(rs.getString("idPatient"));
	        	if (patients.isEmpty() || !patients.contains(pat) ) {
		        	patients.add(pat);
	        	}
	        	
	        	
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return patients;
	}

	public List<Consultation> getConsultations()  {
        String query = "select * from ordonnance where idMedecin=?";
        List<Consultation> ls = new ArrayList<Consultation>();

        try {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1,idE);

        	ResultSet rs = ps.executeQuery();
        

        while (rs.next()) {
            Consultation cons = new Consultation(
            		rs.getString("idPatient"),
            		rs.getString("idMedecin"),
            		rs.getDate("datePrescription"),
            		rs.getString("detail")
            		);
          
            ls.add(cons);
        }
	} catch (SQLException e) {
		e.printStackTrace();
	}
        return ls;
    }


}
