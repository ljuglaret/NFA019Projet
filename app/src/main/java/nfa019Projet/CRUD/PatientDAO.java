package nfa019Projet.CRUD;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nfa019Projet.Modele.Consultation;
import nfa019Projet.Modele.Prescription;
import nfa019Projet.Modele.Roles.Patient;

public class PatientDAO extends Patient {
	Connection conn = null;
	private int nbOrdonnances;
	private String idPatient;
	private List<Consultation> lcons = new ArrayList<Consultation>();
	private String fileConf;
	//Un constructeur permettant d��tablir la connexion avec la base de donn�es en
	//passant par la classe SingleConnection

	public PatientDAO(String fileConf,String idP) {		
		super(idP);
		try {
			this.conn = SingleConnection.getInstance(fileConf);
			this.idPatient = idP.strip();
			this.fileConf = fileConf;

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getFileConf() {
		return fileConf;
	}

	public void setPatientID(String newId) {
		String query = "select * from patient where idPatient = ?  ";
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, idPatient);
			ResultSet rs = ps.executeQuery();
			String SQL = "UPDATE patient SET idPatient = ? WHERE idPatient = ?  ";       
			PreparedStatement statement = conn.prepareStatement(SQL);
			while (rs.next()) {
				statement.setString(1, newId);
				statement.setString(2, idPatient);
				statement.executeUpdate();
			}
			statement.close();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}	
	}


	@Override
	public String getLn() {
		String nom = "";
		String query = "select * from patient where idPatient = ? ";
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, idPatient);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				nom = rs.getString("nom");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return nom;
	}




	@Override
	public String getFn() {
		String prenom = "";
		String query = "select * from patient where idPatient = ? ";
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(query);
			ps.setString(1, idPatient);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				prenom = rs.getString("prenom");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return prenom;
	}

	public void setPatientName(String newName) {
		String query = "select * from patient where idPatient = ?  ";
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, idPatient);
			ResultSet rs = ps.executeQuery();
			String SQL = "UPDATE patient SET nom = ? WHERE idPatient = ?  ";       
			PreparedStatement statement = conn.prepareStatement(SQL);
			while (rs.next()) {
				statement.setString(1, newName);
				statement.setString(2, idPatient);
				statement.executeUpdate();
			}
			statement.close();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}			
	}

	public void setPatientFirstName(String newFirstName) {
		String query = "select * from patient where idPatient = ?  ";
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, idPatient);
			ResultSet rs = ps.executeQuery();
			String SQL = "UPDATE patient SET prenom = ? WHERE idPatient = ?  ";       
			PreparedStatement statement = conn.prepareStatement(SQL);
			while (rs.next()) {
				statement.setString(1, newFirstName);
				statement.setString(2, idPatient);
				statement.executeUpdate();
			}
			statement.close();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}			
	}


	public List<String> getPatientAvecAppareil() {
		List<String> appareils = new ArrayList<String>();
		String query = "select * from patient ";
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, idPatient);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String detail = rs.getString("detail");
				String[] prescriptions = detail.split("\n");
				for(String pres : prescriptions) {
					if(pres.contains("Appareil")) {
						appareils.add(pres.split(" : ")[1]);
					}
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return appareils;
	}
	
	
	public void createPatient(String id, String prenom, String nom, String adr) throws SQLException {
		String query = "INSERT INTO patient(idPatient,prenom,nom,adresse) "
				+ "VALUES(?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1,id);
		ps.setString(2,prenom);
		ps.setString(3,nom);
		ps.setString(4,adr);
		ps.executeUpdate();
		ps.close();
	}

	public List<Consultation> consultations(String idMedecin)  throws SQLException {
		List<Consultation> lcons = new ArrayList<Consultation>();
		String query = "select * from ordonnance where idPatient = ? AND idMedecin = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, idPatient);
		ps.setString(2, idMedecin);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Consultation cons = new Consultation(
					rs.getString("idPatient"),
					rs.getString("idMedecin"),
					rs.getDate("datePrescription"),
					rs.getString("detail")
					);
			lcons.add(cons);
			this.lcons.add(cons);
		}
		nbOrdonnances = lcons.size();
		return lcons;
	}


	//ajout nouvelle ordonnance

	public void addOrdonnance(Consultation consultation)    {

		String SQL = "INSERT INTO ordonnance(idPatient,idMedecin,datePrescription,detail) "
				+ "VALUES(?,?,?,?)";

		try (
				PreparedStatement statement = conn.prepareStatement(SQL);) {

			statement.setString(1,idPatient);
			statement.setString(2,consultation.getIdMedecin());
			statement.setDate(3, consultation.getDate());
			statement.setString(4, "");
			statement.executeUpdate();
			statement.close();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}


	//retrouve la date d'une consultation

	public java.sql.Date getDate(String idMedecin, int idCons){
		Date d = null;
		String query = "select * from ordonnance where idPatient = ? AND idMedecin = ?";
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, idPatient);
			ps.setString(2,idMedecin);


			ResultSet rs = ps.executeQuery();
			int cpt = -1;
			while (cpt != idCons) {
				rs.next();

				d = rs.getDate("datePrescription");
				System.out.println(d);
				cpt++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();;
		}

		return d;

	}

	//ajout prescription � une ordonnance deja existante
	public void addPrescription(String idMedecin, int idCons, Prescription prescription)  throws SQLException  {

		String query = "select * from ordonnance where idPatient = ? AND idMedecin = ? AND datePrescription = ? ";
		PreparedStatement ps = conn.prepareStatement(query);

		ps.setString(1, idPatient);
		ps.setString(2,idMedecin);
		ps.setDate(3, getDate(idMedecin, idCons));


		ResultSet rs = ps.executeQuery();
		String SQL = "UPDATE ordonnance SET detail = ? WHERE idPatient = ? AND idMedecin = ? AND datePrescription = ? ";       
		PreparedStatement statement = conn.prepareStatement(SQL);
		while (rs.next()) {
			statement.setString(1,rs.getString("detail")+prescription.ptoString()+ "\n");
			statement.setString(2, idPatient);
			statement.setString(3,idMedecin);
			statement.setDate(4, getDate(idMedecin, idCons));
			statement.executeUpdate();
			statement.close();
		}
	} 


	public void majOrdonnance(String idMedecin, int idCons, String ordonnanceModifiee)  throws SQLException  {

		String query = "select * from ordonnance where idPatient = ? AND idMedecin = ? AND datePrescription = ? ";
		PreparedStatement ps = conn.prepareStatement(query);

		ps.setString(1, idPatient);
		ps.setString(2,idMedecin);
		ps.setDate(3, getDate(idMedecin, idCons));


		ResultSet rs = ps.executeQuery();
		String SQL = "UPDATE ordonnance SET detail = ? WHERE idPatient = ? AND idMedecin = ? AND datePrescription = ? ";       
		PreparedStatement statement = conn.prepareStatement(SQL);
		while (rs.next()) {
			statement.setString(1,ordonnanceModifiee+ "\n");
			statement.setString(2, idPatient);
			statement.setString(3,idMedecin);
			statement.setDate(4, getDate(idMedecin, idCons));
			statement.executeUpdate();
			statement.close();
		}
	} 


	public int getNbCons() {
		return nbOrdonnances;
	}



}
