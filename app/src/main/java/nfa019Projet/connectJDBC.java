package nfa019Projet;

import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager; 
import java.sql.SQLException;
import java.util.Properties;
 
 
public class connectJDBC {
  
public static void main(String args[]) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver"); 
	        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

			try (InputStream input = classloader.getResourceAsStream("conf/config.properties")) {
	            Properties prop = new Properties();
	            prop.load(input);
	            String user = prop.getProperty("user");
	            String pwd =   prop.getProperty("pwd");
				DriverManager.getConnection("jdbc:mysql://localhost/hopitaldb" ,user,pwd);   
				System.out.println("Connection Established for " + user + " with "+pwd);
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
			
		
		}
		catch(ClassNotFoundException | SQLException e){
			System.err.println(e);
		}
	}
}