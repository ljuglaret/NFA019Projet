# NFA019Projet
[Lien](https://ljuglaret.gitlab.io/NFA019Projet/)
## Présentation

Pour fonctionner le schéma de la base de données doit être le suivant     
-   employes(id,lastname,firstname,city,salary)
-   patient(idPatient,prenom,nom,adresse)
-   ordonnance(idMedecin,idPatient,datePrescription,detail)

Les paramètres de connexion à la base de données se trouvent dans le fichier **congig.properties**.

## Lancement du projet 

java  -jar app/build/libs/app.jar "../config.properties"

java -jar app.jar  "config.properties"

## Organisation

### Interface médecin

Déroulement :
-   Saisie de l'identifiant du médecin
-   Saisie de l'identifiant du patient concerné par la consultation
![](img/med0.png)   

#### Consulter et créer 

![](img/med1.png)   
-   Après avoir selectionné **créer** une nouvelle entrée est ajoutée ) à la table **ordonnance** dans laquelle la colonne **detail** est vide , et la colonne **datePrescription** contient la date du jour.

-   Après avoir selectionné **consulter** on accède à la page : 

#### Imprimer et Modifier
Les ordonnances apparaissent dans le menu déroulant selon leur position dans la table **ordonnance**.   
Le tuple (idPatient,idMedecin,numeroOrdonnance) permet de retrouver la date de l'ordonnance.
![ ](img/med2.png)   

-   Après avoir selectionné **modifier** on peut : 
#### Ajouter une prescription à une ordonnance
![ ](img/medic3.png)   

-   Après avoir selectionné **impression** on obtient le pdf suivant : 
![](img/ordo.png) 
### Interface Agent Administratif
![](img/agt1.png)   

#### Enregistrer nouveau patient
![](img/agt2.png) 

#### Modifier patient
![](img/agt3.png) 